﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

using DataLayer.Data;


namespace DataLayer.Models
{
    public class Person
    {
        public int id { get; set; }

        public string username { get; set; }

        public string userpassword { get; set; }

        public string useremail { get; set; }

        public DateTime createdon { get; set; } = DateTime.Now;

        public bool isdeleted { get; set; } = false;
    }
}
