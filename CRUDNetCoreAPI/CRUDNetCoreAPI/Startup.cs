﻿using Microsoft.AspNetCore.Builder;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DataLayer.Data;
using DataLayer.Models;

using MySql.Data;
using MySql.EntityFrameworkCore.Extensions;

namespace CRUDNetCoreAPI
{
    public class Startup
    {
        public IConfiguration _configuration { get; };

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseMySQL(_configuration.GetConnectionString("DefaultConnection")));
            services.AddControllersWithViews();
        }
    }
}
